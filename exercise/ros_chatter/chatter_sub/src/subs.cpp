#include "ros/ros.h"
#include "std_msgs/String.h"
#include "chatter_sub/chat_msg.h"

void chatterCallback(const chatter_sub::chat_msg& msg)
{
    std::cout << msg.name.c_str() <<" : " << msg.massage.c_str() << std::endl;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "subs");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("chatter", 1000, chatterCallback);


  ros::spin();

  return 0;
}
