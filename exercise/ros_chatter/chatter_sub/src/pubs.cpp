#include "ros/ros.h"
#include "std_msgs/String.h"
#include "chatter_sub/chat_msg.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "pubs");
  ros::NodeHandle nh;

  ros::Publisher chatter_pub = nh.advertise<chatter_sub::chat_msg>("chatter", 1000);

  chatter_sub::chat_msg msg;

  ros::Rate loop_rate(10);
  msg.name = argv[1];
  std::cout << argv[1] << std::endl;

//  ros::Time begin = ros::Time::now();

  while(true)
  {

      std::cout <<"message: ";

      ros::spinOnce();

      std::getline(std::cin, msg.massage);




      chatter_pub.publish(msg);


  }
  return 0;
}
