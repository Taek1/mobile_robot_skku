#include "ros/ros.h"
#include "std_msgs/String.h"
#include "chat/name.h"

void call(const chat::name &ABC)
{
    std::cout <<ABC.A.c_str()<< ABC.B.c_str() << ABC.C.c_str() <<" " << ros::Time::now() << std::endl;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "subscriber_node");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("chatter", 1000, call);

  ros::spin();

  return 0;
}
