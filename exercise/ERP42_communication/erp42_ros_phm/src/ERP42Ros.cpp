#include "ros/ros.h"
#include <cmath>
#include <time.h>
#include "erp42_ros/ERP42_input.h"
#include "erp42_ros/ERP42_feedback.h"
#include "SerialPort.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>


using namespace std;
namespace
{
#pragma pack(push, 1)
struct upper_to_pcu
{
    unsigned char S = 0x53;
    unsigned char T = 0x54;
    unsigned char X = 0x58;
    uint8_t AorM = 0x01;
    uint8_t E_stop = 0x00;
    uint8_t gear = 0x01;
    union speed{    uint8_t speed[2]; uint16_t _speed;};  union speed speed;
    union steer{    char steer[2]; short _steer;};  union steer steer;
    uint8_t brake = 0x50;
    uint8_t alive;
    unsigned char ETX0 = 0x0D;
    unsigned char ETX1 = 0x0A;

};


struct pcu_to_upper
{

    unsigned char S = 0x53;
    unsigned char T = 0x54;
    unsigned char X = 0x58;
    uint8_t  AorM;
    uint8_t  E_stop;
    uint8_t  gear;
    union speed{    uint8_t speed[2]; uint16_t _speed;    }; union speed speed;
    union steer{    char steer[2]; short _steer;   }; union steer steer;
    uint8_t brake;
    union encoder{  int8_t encoder[4]; int32_t _encoder;  }; union encoder encoder;
    uint8_t alive;
    unsigned char ETX0 = 0x0D;
    unsigned char ETX1 = 0x0A;
};
#pragma pack(pop)

enum packet_size : unsigned int
{
    SIZE_P2U = sizeof(pcu_to_upper),
    SIZE_U2P = sizeof(upper_to_pcu),

};
} // namespace

// [  dev/ttyUSB0

class ERP42Interface
{
public:
    ERP42Interface(ros::NodeHandle &n, const char *device_name)
           : pub(n.advertise<erp42_ros::ERP42_feedback>("output", 100)),
             sub(n.subscribe("input", 100, &ERP42Interface::OnInputMsgRecv, this)),
             loop_rate(50), RS232(device_name)
       {
           // open serial device
           // if open device fails, throw.
           // there is no catch for this throw, so this process will be exit with error.
       }
       ~ERP42Interface()
       {
           // close the device
           RS232.Close();
       }

    void OnInputMsgRecv(const erp42_ros ::ERP42_input &msg)
    {

	
	cout << "message received " << endl;
        u2p.AorM = msg.mode;
        u2p.E_stop =  msg.Estop;
        u2p.gear =  msg.gear;
        u2p.speed._speed =  msg.speed_kph * 10;
        u2p.steer._steer =  msg.steer_degree * 71;
        u2p.brake =  msg.brake;
        u2p.alive =  p2u.alive;
	





    }

    void PublishFeedback()
    {
        RS232.Read(PCU_to_UPPER, 18);
        if(PCU_to_UPPER[0] == 0x53 && PCU_to_UPPER[1] == 0x54 && PCU_to_UPPER[2] == 0x58)
                {
                    if(PCU_to_UPPER[16] == 0x0D && PCU_to_UPPER[17] == 0x0A)
                    {
                        p2u.AorM = PCU_to_UPPER[3];
                        p2u.E_stop = PCU_to_UPPER[4];
                        p2u.gear = PCU_to_UPPER[5];
                        p2u.speed.speed[0] = PCU_to_UPPER[6];
                        p2u.speed.speed[1] = PCU_to_UPPER[7];
                        p2u.steer.steer[0] = PCU_to_UPPER[8];
                        p2u.steer.steer[1] = PCU_to_UPPER[9];
                        p2u.brake = PCU_to_UPPER[10];
                        p2u.encoder.encoder[0] = PCU_to_UPPER[11];
                        p2u.encoder.encoder[1] = PCU_to_UPPER[12];
                        p2u.encoder.encoder[2] = PCU_to_UPPER[13];
                        p2u.encoder.encoder[3] = PCU_to_UPPER[14];
                        p2u.alive = PCU_to_UPPER[15];
                    }
                }

//                feedback_msg.mode = (p2u.AorM == 0x00) ? 0 : 1;
                feedback_msg.mode = p2u.AorM;
                feedback_msg.Estop = p2u.E_stop;
                feedback_msg.gear = p2u.gear;
                feedback_msg.speed_kph = (float) p2u.speed._speed / 10;
                feedback_msg.speed_mps = (float) ( (p2u.speed._speed/10) / 1000 ) * 3600;
                feedback_msg.steer_degree = (float) p2u.steer._steer / -71;
                feedback_msg.steer_rad = (float) ( 3.141592 * (p2u.steer._steer/-71) ) / 180;
                feedback_msg.brake = p2u.brake;
                feedback_msg.encoder = p2u.encoder._encoder;
                feedback_msg.alive = p2u.alive;
                pub.publish(feedback_msg);
    }

    void update()
    {
        //PublishFeedback();
	
	u2p.alive = p2u.alive;

        UPPER_to_PCU[0] = u2p.S;
        UPPER_to_PCU[1] = u2p.T;
        UPPER_to_PCU[2] = u2p.X;
        UPPER_to_PCU[3] = u2p.AorM;
        UPPER_to_PCU[4] = u2p.E_stop;
        UPPER_to_PCU[5] = u2p.gear;
        UPPER_to_PCU[6] = u2p.speed.speed[1];
        UPPER_to_PCU[7] = u2p.speed.speed[0];
        UPPER_to_PCU[8] = u2p.steer.steer[1];
        UPPER_to_PCU[9] = u2p.steer.steer[0];
	cout << "u2p.speed.speed: " << static_cast<int>(u2p.speed._speed) << endl;
	cout << "u2p.steer.steer: " << static_cast<int>(u2p.steer._steer) << endl;
        UPPER_to_PCU[10] = u2p.brake;
        UPPER_to_PCU[11] = p2u.alive;
        UPPER_to_PCU[12] = u2p.ETX0;
        UPPER_to_PCU[13] = u2p.ETX1;
	


//        pub.publish(input_msg);
        RS232.Write(UPPER_to_PCU, 14);
    // update() function will be called every 20ms.(50Hz)
    // this is control frequency
	PublishFeedback();
    }

    void spin()
    {
        while (ros::ok())
        {
            update();
            ros::spinOnce();
        }
    }

private:
    ros::Publisher pub;
//    ros::Publisher pub2;
    ros::Subscriber sub;
    ros::Rate loop_rate;
    upper_to_pcu u2p;
    pcu_to_upper p2u;
    erp42_ros::ERP42_feedback feedback_msg;
    erp42_ros::ERP42_input input_msg;
    SerialPort RS232;
    bool msg_received = false;
    unsigned char UPPER_to_PCU[14];
    unsigned char PCU_to_UPPER[18];

};

void print_usage()
{
    ROS_INFO("Need more arguments\n"
             "Usage: ERP42Ros [serial device path]\n");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ERP42Ros");
    if (argc < 2)
    {
        print_usage();
        return -1;
    }
    // now argv[1] is devname
    ros::NodeHandle n;
    ERP42Interface erp42(n, argv[1]);
    erp42.spin();
    return 0;
}
