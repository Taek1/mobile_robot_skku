﻿#include "ros/ros.h"
#include <cmath>
#include <time.h>
#include "erp42_ros/erp42input.h"
#include "erp42_ros/erp42output.h"
#include "SerialPort.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>


using namespace std;
namespace
{
#pragma pack(push, 1)       //check byte by byte
struct upper_to_pcu
{
    unsigned char s = 0x53;
    unsigned char t = 0x54;
    unsigned char x = 0x58;
    unsigned char AorM = 0x01;      // auto_fail_try
    unsigned char estop;
    unsigned char gear;
    union{
        int16_t speed;
        unsigned char __speed[2];
    };
    union{
        int16_t steer;
        unsigned char __steer[2];
    };
    unsigned char brake;
    unsigned char alive;
    unsigned char ext0 = 0x0D;
    unsigned char ext1 = 0x0A;
};

struct pcu_to_upper
{
    unsigned char s = 0x53;
    unsigned char t = 0x54;
    unsigned char x = 0x58;
    unsigned char AorM;
    unsigned char estop;
    unsigned char gear;
    union{
        int16_t speed;
        unsigned char __speed[2];
    };
    union{
        int16_t steer;
        unsigned char __steer[2];
    };
    unsigned char brake;  //1 ~ 200
    union{
        int32_t encoder;
        unsigned char __encoder[4];
    };
    unsigned char alive;
    unsigned char ext0 = 0x0D;
    unsigned char ext1 = 0x0A;
};
#pragma pack(pop)       //check finish
enum packet_size : unsigned int
{
    SIZE_P2U = sizeof(pcu_to_upper),
    SIZE_U2P = sizeof(upper_to_pcu),

};
} // namespace


class ERP42Interface
{
public:
    ERP42Interface(ros::NodeHandle &n, const char*device_name)
        :
//          pub(n.advertise<erp42_ros::ERP42_feedback>("output",100)),
          pub(n.advertise<erp42_ros::erp42output>("output",100)),
          sub(n.subscribe("input",100, &ERP42Interface::OnInputMsgRecv,this)),
          loop_rate(50), RS232(device_name)
//          p2ulog(p2ulog_file.c_str(), ios::app),
//          u2plog(u2plog_file.c_str(), ios::app),
//          p2ubuffer(ringbuf(18))
    {
        // open serial device
        // if open device fails, throw.
        // there is no catch for this throw, so this process will be exit with error.
    }
    ~ERP42Interface()
    {
        RS232.Close();
//        p2ulog.close();
//        u2plog.close();
        // close the device
    }

    void OnInputMsgRecv(const erp42_ros::erp42input &msg)
    {
        input_msg = msg;
    }

//    void PublishFeedback(const erp42_ros::erp42output &fmsg)
    void PublishFeedback(const erp42_ros::erp42output &msg)
    {
        pub.publish(msg);
    }

    void update()
    {
//        struct pcu_to_upper p2u;
        unsigned char pread[18];
//        RS232.Read(pread, 1);    // 1 is checking sequence
//        RS232.Read(pread, 18);
        RS232.Read(pread, SIZE_P2U);      // if '18' do not working
//        std::cout << "hi" <<std::endl;      // testing text1

        if(pread[0] == 0x53 && pread[1] == 0x54 && pread[2] == 0x58 && pread[16] == 0x0D && pread[17] == 0x0A){
//            std::cout << "hihi" <<std::endl;        // testing text2
            p2u.AorM = pread[3];
            p2u.estop = pread[4];
            p2u.gear = pread[5];
            p2u.__speed[0] = pread[6];
            p2u.__speed[1] = pread[7];
            p2u.__steer[0] = pread[8];
            p2u.__steer[1] = pread[9];
            p2u.brake = pread[10];
            p2u.__encoder[0] = pread[11];
            p2u.__encoder[1] = pread[12];
            p2u.__encoder[2] = pread[13];
            p2u.__encoder[3] = pread[14];
            p2u.alive = pread[15];
        }


        feedback_msg.AorM = p2u.AorM;
        feedback_msg.estop = p2u.estop;
        feedback_msg.gear = p2u.gear;
        feedback_msg.speed = (float)(p2u.speed / 10);
        feedback_msg.steer = (float)(p2u.steer / (-71));
        feedback_msg.brake = p2u.brake;
        feedback_msg.encoder = p2u.encoder;
        feedback_msg.alive = p2u.alive;

        PublishFeedback(feedback_msg);


        OnInputMsgRecv(input_msg);

        u2p.AorM = input_msg.AorM;
        u2p.estop = input_msg.estop;
        u2p.gear = input_msg.gear;
        u2p.speed = (float)(input_msg.speed * 10);
        u2p.steer = (float)(input_msg.steer * (-71));
        u2p.brake = input_msg.brake;
        u2p.alive = p2u.alive;

        unsigned char write[14];

        write[0] = u2p.s;
        write[1] = u2p.t;
        write[2] = u2p.x;
        write[3] = u2p.AorM;
        write[4] = u2p.estop;
        write[5] = u2p.gear;
        write[6] = u2p.__speed[1];
        write[7] = u2p.__speed[0];
        write[8] = u2p.__steer[1];
        write[9] = u2p.__steer[0];
        write[10] = u2p.brake;
        write[11] = u2p.alive;
        write[12] = u2p.ext0;
        write[13] = u2p.ext1;

//        RS232.Write(write, 14);
        RS232.Write(write, SIZE_U2P);       //if '14' do not working
//        ros::Rate loop_rate(50);


    // update() function will be called every 20ms.(50Hz)
    // this is control frequency
    }

    void spin()
    {
        while (ros::ok())
        {
            update();
            ros::spinOnce();
            //loop_ratepread.sleep();
        }
    }

private:
    ros::Publisher pub;
    ros::Subscriber sub;
    ros::Rate loop_rate;
    upper_to_pcu u2p;
    pcu_to_upper p2u;
//    unsigned char pread[18];
//    unsigned char write[14];
    erp42_ros::erp42output feedback_msg;
    erp42_ros::erp42input input_msg;
    SerialPort RS232;
};

void print_usage()
{
    ROS_INFO("Need more arguments\n"
             "Usage: ERP42Ros [serial device path]\n");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ERP42Ros");
    if (argc < 2)
    {
        print_usage();
        return -1;
    }
    // now argv[1] is devname
    ros::NodeHandle n;
    ERP42Interface erp42(n, argv[1]);
    erp42.spin();
    return 0;
}
