#include "ros/ros.h"
#include <cmath>
#include <time.h>
#include "erp42ros/ERP42_input.h"
#include "erp42ros/ERP42_feedback.h"
#include "SerialPort.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>


using namespace std;
namespace
{
    struct upper_to_pcu
    {
    unsigned char stx[3] = {0x53, 0x54, 0x58};
    unsigned char AorM = 0x01; 
    unsigned char E_Stop =0x01; 
    unsigned char gear = 0x00;    

    union {
        unsigned char speed0;
        unsigned char speed1;
    };
    union {
        unsigned char steer0;
        unsigned char steer1;
    };
    unsigned char brake = 0x01;
    unsigned char alive = 0x00;
    unsigned char ext[2] = {0x0d, 0x0a};
    };

struct pcu_to_upper
{
    unsigned char stx[3] = {0x53, 0x54, 0x58};
    unsigned char AorM ; 
    unsigned char E_Stop;
    unsigned char gear ;    
    union {
        unsigned char speed0;
        unsigned char speed1;
    };
    union {
        unsigned char steer0;
        unsigned char steer1;
    };
    unsigned char brake ;
    char enc[4];
    unsigned char alive;
    unsigned char ext[2] = {0x0d, 0x0a};
};

/*
enum packet_size : unsigned int
{
    SIZE_P2U = sizeof(pcu_to_upper);
    SIZE_U2P = sizeof(upper_to_pcu);
};*/

} // namespace


class ERP42Interface
{
public:
    ERP42Interface(ros::NodeHandle &n, const char *device_name)
           : pub(n.advertise<erp42ros::ERP42_feedback>("output", 100)),
             sub(n.subscribe("input", 100, &ERP42Interface::OnInputMsgRecv, this)),
             loop_rate(50), RS232(device_name)
       {
        //
           // open serial device
           // if open device fails, throw.
           // there is no catch for this throw, so this process will be exit with error.
       }
       ~ERP42Interface()
       {
           // close the device
           RS232.Close();

   }
    void OnInputMsgRecv(const erp42ros::ERP42_input &msg)
    {
        input_msg =msg;
       //
        for(int i=0;i<10000;i++)
        {
            if(i=5000)
            input_msg.steer_rad=2000;
            if(i=9999)
                input_msg.steer_rad=-2000;
        }
    }

    void PublishFeedback()
    {
        pub.publish(feedback_msg);
    }

    void check_bit(unsigned char *buf)
    {
        RS232.Read(buf,18);
        cout<<buf<<"!!"<<endl;
        if(buf[0]==0x53&&buf[1]==0x54&&buf[2]==0x58)
            cout<<"i got it"<<endl;
    }

    void update()
        {
//input_pack is package for input
//feed_pack is package for feedback

            input_pack[3] = u2p.AorM;
            input_pack[4] = u2p.E_Stop;
            input_pack[5] = u2p.gear;
            input_pack[6] = u2p.speed0;
            input_pack[7] = u2p.speed1;
            input_pack[8] = u2p.steer0;
            input_pack[9] = u2p.steer1;
            input_pack[10] = u2p.brake;
            input_pack[11] = u2p.alive;

            //unsigned char* input_package = (unsigned char *)&u2p;
            RS232.Write(input_pack, 14);

            feedback_msg.mode=feed_pack[3];
            feedback_msg.Estop=feed_pack[4];
            feedback_msg.gear=feed_pack[5];
            box=0;
            box=((0xFF && feed_pack[7])<<8)+(0xff && feed_pack[6]);
            std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[6]) << std::endl;
             std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[7]) << std::endl;
              std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[8]) << std::endl;
               std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[9]) << std::endl;
                std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[10]) << std::endl;
                 std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[11]) << std::endl;
                  std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[12]) << std::endl;
                   std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[13]) << std::endl;
                    std::cout << "feed_pack[7] : " << static_cast<int>(feed_pack[14]) << std::endl;



            feedback_msg.speed_kph=(( feed_pack[7] & 0xff )<<8)| ((feed_pack[6] & 0xff)<< 0 );
            //feedback_msg.speed_mps=feed_pack[6];

            box=0;
            box=((0xFF && feed_pack[9])<<8)+ (0xff && feed_pack[8]);

            feedback_msg.steer_rad=((0xFF & feed_pack[9])<<8)| (0xff & feed_pack[8]);
            feedback_msg.brake=feed_pack[10];
            box=0;
            box=(( 0xff && feed_pack[14])<<24) + (( 0xff && feed_pack[13])<<16) + (( 0xff && feed_pack[12])<<8) + (( 0xff && feed_pack[11])<<0);
            feedback_msg.encoder = (( 0xff & feed_pack[14])<<24) | (( 0xff & feed_pack[13])<<16) | (( 0xff & feed_pack[12])<<8) | (( 0xff & feed_pack[11])<<0);
            check_bit(feed_pack);

    }

    void spin()
    {
        while (ros::ok())
        {

            update();
            PublishFeedback();
            ros::spinOnce();
            //loop_rate.sleep();
        }
    }

private:
    ros::Publisher pub;
    ros::Subscriber sub;
    ros::Rate loop_rate;
    upper_to_pcu u2p;
    pcu_to_upper p2u;
    erp42ros::ERP42_feedback feedback_msg;
    erp42ros::ERP42_input input_msg;
    SerialPort RS232;
    unsigned char input_pack[14];
    unsigned char feed_pack[18];
    float box;

};

void print_usage()
{
    ROS_INFO("Need more arguments\n"
             "Usage: ERP42Ros [serial device path]\n");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ERP42Ros");
    if (argc < 2)
    {
        print_usage();
        return -1;
    }
    // now argv[1] is devname
    ros::NodeHandle n;
    //
    ERP42Interface erp42(n, argv[1]);
    //
    erp42.spin();
    return 0;
}
