#include "ros/ros.h"
#include <cmath>
#include <time.h>
#include "erp42_ros/ERP42_input.h"
#include "erp42_ros/ERP42_feedback.h"
#include "SerialPort.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>

using namespace std;
namespace
{
#pragma pack(push, 1)
struct upper_to_pcu
{
    unsigned char S = 0x53;
    unsigned char T = 0x54;
    unsigned char X = 0x58;
    int8_t mode = 0x01;
    bool Estop;
    int8_t gear;
    int16_t speed;
    int16_t steer;
    uint8_t brake;
    uint8_t alive;
    unsigned char ETX0 = 0x0D;
    unsigned char ETX1 = 0x0A;
};

struct pcu_to_upper
{
    unsigned char S = 0x53;
    unsigned char T = 0x54;
    unsigned char X = 0x58;
    int8_t mode;
    bool Estop;
    int8_t gear;
    int16_t speed;
    int16_t steer;
    uint8_t brake;
    int32_t encoder;
    uint8_t alive;
    unsigned char ETX0 = 0x0D;
    unsigned char ETX1 = 0x0A;
};
#pragma pack(pop)

enum packet_size : unsigned int
{
    SIZE_P2U = sizeof(pcu_to_upper),
    SIZE_U2P = sizeof(upper_to_pcu),

};
} // namespace


class ERP42Interface
{
public:
    ERP42Interface(ros::NodeHandle &n, const char *device):
//    ERP42Interface(ros::NodeHandle &n, const char *device):
    pub(n.advertise<erp42_ros::ERP42_feedback>("output",100)),
    sub(n.subscribe("input",100,&ERP42Interface::OnInputMsgRecv,this)),
    loop_rate(50),
    RS232(device){
    }
//    {
//        RS232.Open(device);
//        pub(n.advertise<mobile_robot_ERP42::ERP42_feedback>("output",100));
//        sub(n.subscribe("input",100,&ERP42Interface::OnInputMsgRecv,this));
//        loop_rate(50);
//        RS232(device);
        // open serial device

        // if open device fails, throw.
        // there is no catch for this throw, so this process will be exit with error.
//    }
    ~ERP42Interface()
    {
        // close the device
        RS232.Close();
    }

//    void OnInputMsgRecv(const erp42_ros::ERP42_input &msg)
    void OnInputMsgRecv(const erp42_ros::ERP42_input &msg)
    {
        input_msg = msg;
        u2p.mode = (int8_t) (input_msg.mode);
        u2p.Estop = input_msg.Estop;
        u2p.gear = (int8_t) input_msg.gear;
        u2p.speed = (int16_t) (input_msg.speed_kph*10);
        u2p.steer = (int16_t) (input_msg.steer_degree*71);
        u2p.brake = (int8_t) input_msg.brake;

    }

    void PublishFeedback()
    {

        RS232.Read(p2u_buf, 18);
//        std::cout << "input msg recv : "<< p2u_buf <<endl;

//        std::cout << "p2u alive before : "<< p2u.alive <<endl;
        if(p2u_buf[0] == 0x53 && p2u_buf[1] == 0x54 && p2u_buf[2] == 0x58)
        {
            if(p2u_buf[16] == 0x0D && p2u_buf[17] == 0x0A)
            {
                p2u.mode = p2u_buf[3];
                p2u.Estop = p2u_buf[4];
                p2u.gear = p2u_buf[5];
                unsigned char temp1 = p2u_buf[6]&0xFF;
                unsigned char temp2 = p2u_buf[7]&0xFF;
                p2u.speed = uint16_t((temp2 << 8) + (temp1 << 0));
                unsigned char temp3 = p2u_buf[8]&0xFF;
                unsigned char temp4 = p2u_buf[9]&0xFF;
                p2u.steer = -(int16_t)((temp4 << 8) + (temp3 << 0));
                p2u.brake = p2u_buf[10];
                temp1 = p2u_buf[11]&0xFF;
                temp2 = p2u_buf[12]&0xFF;
                temp3 = p2u_buf[13]&0xFF;
                temp4 = p2u_buf[14]&0xFF;
                p2u.encoder = int32_t((temp4 << 24) + (temp3 << 16) + (temp2 << 8) + (temp1 << 0));
                p2u.alive = p2u_buf[15];

            }
        }
//        std::cout << "p2u alive after : "<< p2u.alive <<endl;
        feedback_msg.mode = p2u.mode;
        feedback_msg.Estop = p2u.Estop;
        feedback_msg.gear = p2u.gear;
        feedback_msg.speed_kph = static_cast< double_t > (p2u.speed/10.0);
        feedback_msg.speed_mps = feedback_msg.speed_kph *1000/3600;
        feedback_msg.steer_degree = static_cast< double_t > (p2u.steer/71.0);
        feedback_msg.steer_rad = feedback_msg.steer_degree * 3.141592 /180;
        feedback_msg.brake = p2u.brake;
        feedback_msg.encoder = p2u.encoder;
        feedback_msg.alive = p2u.alive;
        pub.publish(feedback_msg);
    }

    void update()
    {
    // update() function will be called every 20ms.(50Hz)
    // this is control frequency
        u2p.alive = p2u.alive;
//        memcpy(u2p_buf, &u2p, sizeof(u2p));
        u2p_buf[0] = u2p.S;
        u2p_buf[1] = u2p.T;
        u2p_buf[2] = u2p.X;
        u2p_buf[3] = u2p.mode;
        u2p_buf[4] = u2p.Estop;
        u2p_buf[5] = u2p.gear;
        u2p_buf[6] = (u2p.speed >> 8);
        u2p_buf[7] = u2p.speed;
        u2p_buf[8] = (u2p.steer >> 8);
        u2p_buf[9] = u2p.steer;
        u2p_buf[10] = u2p.brake;
        u2p_buf[11] = u2p.alive;
        u2p_buf[12] = u2p.ETX0;
        u2p_buf[13] = u2p.ETX1;
//        std::cout << "u2p alive send : "<< u2p.alive <<endl;


        RS232.Write(u2p_buf, SIZE_U2P);

        PublishFeedback();
    }

    void spin()
    {
        while (ros::ok())
        {
            update();
            ros::spinOnce();
            //loop_rate.sleep();
        }
    }

private:
    ros::Publisher pub;
    ros::Subscriber sub;
    ros::Rate loop_rate;
    upper_to_pcu u2p;
    pcu_to_upper p2u;
    erp42_ros::ERP42_feedback feedback_msg;
    erp42_ros::ERP42_input input_msg;
    SerialPort RS232;
    uint8_t alive;
    unsigned char u2p_buf[14];
    unsigned char p2u_buf[18];
};

void print_usage()
{
    ROS_INFO("Need more arguments\n"
             "Usage: ERP42Ros [serial device path]\n");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ERP42Ros");
    if (argc < 2)
    {
        print_usage();
        return -1;
    }
    // now argv[1] is devname
    ros::NodeHandle n;
    ERP42Interface erp42(n, argv[1]);
    erp42.spin();
    return 0;
}
