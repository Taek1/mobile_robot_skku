#include "ros/ros.h"
#include "std_msgs/String.h"
#include "erp42_ros/ERP42_input.h"
#include <iostream>

using namespace std;

//ERP42_MAX_LIN_VEL = 20;
//ERP42_MAX_ANG_VEL = 28;

//LIN_VEL_STEP_SIZE = 0.5;
//ANG_VEL_STEP_SIZE = 1;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "erp42_teleop_key");
    ros::NodeHandle nh;

    ros::Publisher chatter_pub = nh.advertise<erp42_ros::ERP42_input>("input", 1000);
    erp42_ros::ERP42_input msg;
    ros::Rate loop_rate(50);
    string name;
    string value;

    msg.mode = 1;
    msg.Estop = 0;
    msg.gear = 1;
    msg.speed_kph = 0;
    msg.steer_degree = 0;
    msg.brake = 50;

    while (ros::ok())
    {
        cout << "1(AorM), 2(Estop), 3(Gear), 4(Speed), 5(Steer), 6(Brake), 9(end): ";
        cin >> name;

        if(name == "1" || name == "AorM" || name == "aorm") {
            cout << "AorM - 0 : Manual, 1 : Auto = ";
            cin >> value;
            msg.mode = stoi(value);
        } else if (name == "2" || name == "Estop" || name == "estop") {
            cout << "Estop - 0 : Estop off, 1 : Estop on = ";
            cin >> value;
            msg.Estop = boost::lexical_cast<bool>(value);
        } else if (name == "3" || name == "Gear" || name == "gear") {
            cout << "Gear - 0 : forward, 1 : neutral, 2 : backward = ";
            cin >> value;
            msg.gear = stoi(value);
        } else if (name == "4" || name == "Speed" || name == "speed") {
            cout << "Speed - 0~20 km/s = ";
            cin >> value;
            msg.speed_kph = stof(value);
        } else if (name == "5" || name == "Steer" || name == "steer") {
            cout << "Steer - -28 ~ 28 degree = ";
            cin >> value;
            msg.steer_degree = stof(value);
        } else if (name == "6" || name == "Brake" || name == "brake") {
            cout << "Brake - 1 ~ 200 = ";
            cin >> value;
            msg.brake = stoi(value);
        } else if(name == "9"){
            msg.mode = 0;
            msg.Estop = 1;
            msg.gear = 1;
            msg.speed_kph = 0;
            msg.steer_degree = 0;
            msg.brake = 50;
           return 0;
        } else {
            msg = msg;
        }

        cout << "AorM : " << msg.mode << endl;
        cout << "E-stop : " << (bool) msg.Estop << endl;
        cout << "Gear : " << msg.gear << endl;
        cout << "Speed : " << msg.speed_kph << endl;
        cout << "Steer : " << msg.steer_degree << endl;
        cout << "Brake : " << msg.brake << endl;


        chatter_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}
