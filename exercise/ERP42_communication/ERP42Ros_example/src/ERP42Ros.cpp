#include "ros/ros.h"
#include <cmath>
#include <time.h>
#include "erp42_ros/ERP42_input.h"
#include "erp42_ros/ERP42_feedback.h"
#include "SerialPort.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>


using namespace std;
namespace
{
struct upper_to_pcu
{
};

struct pcu_to_upper
{
};

enum packet_size : unsigned int
{
    SIZE_P2U = sizeof(pcu_to_upper),
    SIZE_U2P = sizeof(upper_to_pcu),

};
} // namespace


class ERP42Interface
{
public:
    ERP42Interface(ros::NodeHandle &n, const char*device_name)
        :
          pub(n.advertise<erp42_ros::ERP42_feedback>("output",100)),
          sub(n.subscribe("input",100, &ERP42Interface::OnInputMsgRecv,this)),
          loop_rate(50), RS232(device_name),
//          p2ulog(p2ulog_file.c_str(), ios::app),
//          u2plog(u2plog_file.c_str(), ios::app),
          p2ubuffer(ringbuf(18))
    {
        // open serial device
        // if open device fails, throw.
        // there is no catch for this throw, so this process will be exit with error.
    }
    ~ERP42Interface()
    {
        RS232.Close();
//        p2ulog.close();
//        u2plog.close();
        // close the device
    }

    void OnInputMsgRecv(const erp42_ros::ERP42_input &msg)
    {
    }

    void PublishFeedback()
    {

    }

    void update()
    {
    // update() function will be called every 20ms.(50Hz)
    // this is control frequency
    }

    void spin()
    {
        while (ros::ok())
        {
            update();
            ros::spinOnce();
            //loop_rate.sleep();
        }
    }

private:
    ros::Publisher pub;
    ros::Subscriber sub;
    ros::Rate loop_rate;
    upper_to_pcu u2p;
    pcu_to_upper p2u;
    erp42_ros::ERP42_feedback feedback_msg;
    erp42_ros::ERP42_input input_msg;
    SerialPort RS232;
};

void print_usage()
{
    ROS_INFO("Need more arguments\n"
             "Usage: ERP42Ros [serial device path]\n");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ERP42Ros");
    if (argc < 2)
    {
        print_usage();
        return -1;
    }
    // now argv[1] is devname
    ros::NodeHandle n;
    ERP42Interface erp42(n, argv[1]);
    erp42.spin();
    return 0;
}
