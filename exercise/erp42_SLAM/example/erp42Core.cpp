#include <ros/ros.h>
#include <ros/time.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Twist.h>
#include <tf/tf.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <boost/scoped_ptr.hpp>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include "erp42_slam/ERP42_feedback.h"

//TODO: Define some global variables
using namespace std;

class erp42Core
{
public:
    erp42Core(ros::NodeHandle &nh)
    {
        init();
    }
    void init()
    {
        if(!nh.getParam("wheel_base", wheel_base_))
            wheel_base_ = 1.05;
         if(!nh.getParam("car_width", car_width_))
            car_width_ = 0.99;
         if(!nh.getParam("max_vel", max_vel_))
            max_vel_ = 5.55;
         if(!nh.getParam("min_vel", min_vel_))
            min_vel_ = -5.55;
         if(!nh.getParam("max_steering_angle", max_steering_angle_))
            max_steering_angle_ = 28.169;
         if(!nh.getParam("min_steering_angle", min_steering_angle_))
            min_steering_angle_ = -28.169;

    }

    void update()
    {
    }

    void spin()
    {
        while(ros::ok())
        {
        }
    }

    void initOdom(void)
    {
    }

    void publishDriveInformation(void)
    {
    }

    void calcOdometry(double diff_time)
    {
    }

    void updateOdometry(void)
    {
    }

    void updateTF(geometry_msgs::TransformStamped& odom_tf)
    {
    }

    void commandVelocityCallback(const geometry_msgs::Twist &cmd_vel_msg)
    {
    }
    
    void recvFeedback(const erp42_slam::ERP42_feedback &msg)
    {
    }


private:
    /*Subscriber */
    ros::Subscriber cmd_vel_sub;
    ros::Subscriber feedback_sub;
    /*Publisher */
    nav_msgs::Odometry odom;
    ros::Publisher odom_pub;

    /*Transform Broadcaster */
    geometry_msgs::TransformStamped odom_tf;
    tf2_ros::TransformBroadcaster tf_broadcaster_;

    /*Parameter*/
    erp42_slam::ERP42_feedback feedback_msg;
    float goal_velocity_from_cmd[2] = {0.0, 0.0};
    float odom_pose[3];
    double odom_vel[3];

    ros::Rate loop_rate;

    double wheel_base_;
    double car_width_;
    double max_vel_;
    double min_vel_;
    double max_steering_angle_;
    double min_steering_angle_;
    ros::NodeHandle nh;
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "erp42_core");
    ros::NodeHandle nh;
    erp42Core core(nh);
    core.spin();
    return 0;
}
