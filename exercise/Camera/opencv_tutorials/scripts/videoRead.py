#!/usr/bin/env python
import cv2

cap = cv2.VideoCapture(0)

print 'width: {0}, height: {1}'.format(cap.get(3),cap.get(4))

while(True):
    ret, frame = cap.read()

    if (ret):

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == 27:
            break

cap.release()
cv2.destroyAllWindows()