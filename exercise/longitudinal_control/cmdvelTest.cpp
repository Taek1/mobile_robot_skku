#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include <iostream>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "cmdvelTest");
    ros::NodeHandle n;

    ros::Publisher velPub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
    ros::Rate loop_rate(20);

    geometry_msgs::Twist msg;
    msg.linear.x = 0.5;

    ros::Time beginTime = ros::Time::now();
    ros::Duration duration = ros::Duration(5);
    ros::Time endTime = beginTime + duration;

    while (ros::Time::now() < endTime)
    {
        velPub.publish(msg);
        loop_rate.sleep();
    }
    msg.linear.x = 0.0;
    velPub.publish(msg);
    return 0;
}